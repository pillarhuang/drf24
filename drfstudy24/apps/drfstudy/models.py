from django.db import models

# Create your models here.
# 学生模型类
class Student(models.Model):
    SEX_CHOICES = (
        (0,'女'),
        (1,'男')
    )
    name = models.CharField(max_length=30,verbose_name='名字') #管理后台应用的
    age = models.IntegerField(null=True,blank=True,verbose_name='年龄')
    sex = models.IntegerField(null=True,blank=True,verbose_name='性别',
                              choices=SEX_CHOICES)
    create_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True,verbose_name='更新时间')
    is_delete = models.BooleanField(default=False,verbose_name='逻辑删除')
    classes = models.ForeignKey('Classes',on_delete=models.CASCADE,verbose_name='班级')
    # 重写delete
    def delete(self, using=None, keep_parents=False):
        self.is_delete = True
        self.save()

# 班级模型类
class Classes(models.Model):
    name = models.CharField(max_length=30,verbose_name='班级名字')
    slogn = models.TextField(null=True,blank=True,verbose_name='班级口号')
    create_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True,verbose_name='更新时间')
    is_delete = models.BooleanField(default=False,verbose_name='逻辑删除')
    # 重写delete
    def delete(self, using=None, keep_parents=False):
        self.is_delete = True
        self.save()
    # 输出格式
    def __str__(self):
        return self.name #返回自己的班级名字进行展示输出