from django.urls import path
from .views import *
from rest_framework.routers import DefaultRouter, SimpleRouter #默认路由器

# urlpatterns = [
#     path('students/',StudentsViewSet.as_view({'get':'list','post':'create'})),
#     path('students/<pk>',StudentsViewSet.as_view({'get':'retrieve','put':'update','delete':'destory'}))
# ]

urlpatterns = [
]
#创建路由器
router = DefaultRouter()
# router = SimpleRouter()
#注册路由
router.register('students',StudentViewSet)
router.register('classes',ClassesViewSet)
# 追加到路由列表中
urlpatterns += router.urls