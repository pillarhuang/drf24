from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response #响应json数据
from rest_framework.decorators import action
# from .models import *
from .serializers import * #序列化器会把相关联的模型类全部导入进来
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_201_CREATED,HTTP_204_NO_CONTENT

# Create your views here.
#     视图集：封装了一套响应方式
#     定义：
#         create创建数据，list查询所有，retrieve单条查询，update修改，delete删除
# class StudentsViewSet(viewsets.ViewSet):
#     # list() 提供一组数据
#     def list(self,request):
#         students = Students.objects.all()  #查询所有，  查询集
#         #序列化操作 ==》 json
#         serializer = StudentsSerializer(students,many=True)
#         return Response(serializer.data) #数据
#     # create
#     def create(self,request):
#         serializer = StudentsSerializer(data=request.data) # 前端数据反序列化
#         # 校验数据
#         serializer.is_valid(raise_exception=True)
#         # 保存
#         serializer.save()
#         return Response(serializer.data)
#     # retrieve() 提供单个数据
#     def retrieve(self,request,pk):
#         try:
#             students = Students.objects.get(id=pk)  # 查询单个
#         except students.DoesNotExist:
#             return Response(status=HTTP_404_NOT_FOUND)
#         # 序列化操作 ==》 json
#         serializer = StudentsSerializer(students)
#         return Response(serializer.data)  # 返回单条数据
#     # UPDATE
#     def update(self,request,pk):
#         try:
#             students = Students.objects.get(id=pk)  # 查询单个
#         except students.DoesNotExist:
#             return Response(status=HTTP_404_NOT_FOUND)
#         #反序列化，原模型数据，前端传过来的数据
#         serializer = StudentsSerializer(students,data=request.data)  # 前端数据反序列化
#         # 校验数据模型
#         serializer.is_valid(raise_exception=True)
#         # 保存反序列化数据，这里不是保存table数据
#         serializer.save()
#         return Response(serializer.data)
#     # 删除
#     def destory(self,request,pk):
#         try:
#             students = Students.objects.get(id=pk)  # 查询单个
#         except students.DoesNotExist:
#             return Response(status=HTTP_404_NOT_FOUND)
#         #删除
#         students.delete()
#         return Response(status=HTTP_204_NO_CONTENT)
#
class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.filter(is_delete=False) #查询逻辑删除为False的值
    serializer_class = StudentSerializer #指定使用的序列化器

# 班级视图类
class ClassesViewSet(viewsets.ModelViewSet):
    queryset = Classes.objects.filter(is_delete=False) #查询逻辑删除为False的值
    serializer_class = ClassesSerializer #指定使用的序列化器
    # 行为方法
    @action(methods=['get'],detail=False)  #false默认不需要多的参数
    def last(self,request):
        classes = Classes.objects.last() #模型当中最后一行
        return Response(self.get_serializer(classes).data) #直接使用当前class定义的序列化器，即serializer_class