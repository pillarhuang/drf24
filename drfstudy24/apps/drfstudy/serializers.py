from rest_framework import serializers #序列化器
from .models import *
#学生的序列化器
#序列化器最好是模型类名+serializser
# class StudentsSerializer(serializers.Serializer):
    # id = serializers.CharField(label='ID',read_only=True)
    # name = serializers.CharField(max_length=30,label='姓名')
    # age = serializers.IntegerField(label='年龄',required=False)
    # sex = serializers.IntegerField(label='性别',)
    # create_time = serializers.DateTimeField(required=False,label='创建时间')
    # update_time = serializers.DateTimeField(required=False,label='更新时间')
    # is_delete = serializers.BooleanField(default=False,label='逻辑删除',required=False)
    # def create(self,validate_data): # 字典， 表单数据  {"name":"zl","age":18,"sex":1}
    #     return Students.objects.create(**validate_data)
    # #重写update
    # def update(self, instance, validated_data):
    #     instance.name = validated_data.get('name',instance.name) #如果没有数据，给原模型的值
    #     instance.age = validated_data.get('age',instance.age) #如果没有数据，给原模型的值
    #     instance.sex = validated_data.get('sex',instance.sex) #如果没有数据，给原模型的值
    #     instance.save()
    #     return instance
class StudentSerializer(serializers.ModelSerializer):
    class_name = serializers.CharField(source='classes.name',read_only=True)
    class Meta:
        model = Student #模型类==》序列化字段
        #指定映射哪些字段
        # fields = '__all__'
        # fields = ["name","age","sex"]
        # 不想映射哪些字段，与fields只能用一个
        exclude = ["is_delete"]

        #数据校验 age: 0-100
        extra_kwargs = {
            'age':{'min_value':0,'max_value':100}
        }
    #针对名字做属性级别校验
    def validate_name(self,value):
        if 'mq' not in value.lower():
            raise serializers.ValidationError('用户名需要包含mq')
        return value

    # 对象级别校验
    def validate(self, data):
        if 'mq' not in data['name'].lower():
            raise serializers.ValidationError('用户名需要包含mq')
        if data['age'] < 18:
            raise  serializers.ValidationError('未成年人进制访问')
        return data
#新增一个简单序列化器，一定要定义在引用它的序列化器的前面
class StudentSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Student #模型类==》序列化字段
        #指定映射哪些字段
        # fields = '__all__'
        fields = ["name","age","sex"]
        # 不想映射哪些字段，与fields只能用一个
        # exclude = ["is_delete"]

class ClassesSerializer(serializers.ModelSerializer):
    # 新增字段，嵌套序列化，展示所有数据
    student_set = StudentSerializerSimple(many=True,read_only=True)
    class Meta:
        model = Classes #模型类==》序列化字段
        #指定映射哪些字段
        # fields = '__all__'
        # fields = ["name","age","sex"]
        # 不想映射哪些字段，与fields只能用一个
        exclude = ["is_delete"]

